﻿using System;
using Mrg.RegistrationApplicationService;
using Xunit.Abstractions;

namespace Mrg.Tests
{
    public abstract class DomainTestBase : IDisposable
    {
        private readonly ITestOutputHelper _output;
        protected readonly ApplicationClient Client;

        protected DomainTestBase(ITestOutputHelper output)
        {
            _output = output;
            this.Client = MrgApplication.Create().ConfigureInMemoryStore().Run();
        }

        public void Dispose()
        {
            this.Client.Dispose();
        }
    }
}