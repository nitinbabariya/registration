﻿using EventFlow.Queries;
using Mrg.ApplicationService.Registration;
using Mrg.Domain.Registration;
using Mrg.ReadStore.Registration;

namespace Mrg.ApplicationService.Client
{
    public sealed class RegistrationModel : ClientModelBase<RegistrationReadModel, RegistrationId>
    {
        public RegistrationModel(RegistrationId id, ApplicationClient client) : base(client, id) { }

        public RegistrationModel(RegistrationReadModel entity, ApplicationClient client) : base(client, new RegistrationId(entity.Id.Value), entity)
        { }

        public string Name => this.ReadModel.FirstName + " " + this.ReadModel.LastName;
        public Address Address =>  this.ReadModel.Address;
        public string PersonalNumber => this.ReadModel.PersonalNumber;
        public string FavoriteFootballTeam => this.ReadModel.FavoriteFootballTeam;

        protected override RegistrationReadModel QueryReadModel()
        {
            return this.Query(new ReadModelByIdQuery<RegistrationReadModel>(this.Id));
        }
    }
}
