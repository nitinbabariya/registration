﻿using System;
using Autofac;
using EventFlow;
using EventFlow.Autofac.Extensions;
using EventFlow.EventStores.Files;
using EventFlow.Extensions;
using Mrg.Domain;
using Mrg.ReadStore;
using Mrg.ReadStore.Registration;

namespace Mrg.ApplicationService
{
    public sealed class MrgApplication : IConfigureApplication
    {
        private readonly Lazy<IContainer> _container;
        private readonly IEventFlowOptions _options;

        private MrgApplication()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ApplicationClient>().AsSelf();

            builder.RegisterAssemblyTypes(GetType().Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces();

            _options = EventFlowOptions.New
                .UseAutofacContainerBuilder(builder).AddDomain();

            _container = new Lazy<IContainer>(() => _options.CreateContainer());
        }

        public MrgApplication Configure(Action<IEventFlowOptions> configuration)
        {
            configuration(_options);
            return this;
        }

        public ApplicationClient Run()
        {
            return _container.Value.Resolve<ApplicationClient>();
        }

        public IConfigureApplication ConfigureInMemoryStore()
        {
            UseInMemoryStore();
            return this;
        }

        public static MrgApplication Create()
        {
            return new MrgApplication();
        }

        private void UseInMemoryStore()
        {
            _options
                .AddQueryHandlers(ReadStoreConfiguration.GetAssembly)
                .UseInMemoryReadStoreFor<RegistrationReadModel>();
        }

        private void UseFileSystemBasedStore(string storePath)
        {
            var fileEventStoreConfiguration = FilesEventStoreConfiguration.Create(storePath);
            _options.UseFilesEventStore(fileEventStoreConfiguration)
                .AddQueryHandlers(ReadStoreConfiguration.GetAssembly)
                .UseInMemoryReadStoreFor<RegistrationReadModel>();
        }
    }
}