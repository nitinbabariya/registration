﻿namespace Mrg.ApplicationService.Registration.RegistrationForm
{
    /*
     * An alternate solution is to have RegistrationFormBuilder as an abstract class
     * and create two different builder implementations i.e. one builds with a favoriteteam
     * and another could build with personal number
     *
     * Following KISS - keeping it as only one implementation
     */
    public class RegistrationFormBuilder
    {
        private RegistrationForm _registrationForm;

        private RegistrationFormBuilder()
        {
        }

        public static RegistrationFormBuilder Begin(string firstname,string lastName,string address)
        {
            return new RegistrationFormBuilder
            {
                _registrationForm = new RegistrationForm
                {
                    FirstName = firstname,
                    LastName = lastName,
                    Address = new Address(address),
                    PersonalNumber = PersonalNumber.NotSpecified,
                    FavoriteFootballTeam = FavoriteFootballTeam.NotSpecified
                }
            };
        }

        public  RegistrationFormBuilder WithPersonalNumber(string personalNumber)
        {
            _registrationForm.PersonalNumber = personalNumber;
            return this;
        }

        public RegistrationFormBuilder WithFavoriteFootballTeam(string favoriteFootballTeam)
        {
            _registrationForm.FavoriteFootballTeam= favoriteFootballTeam;
            return this;
        }

        public RegistrationForm Build()
        {
            /* TODO: Could have a validation so a registrationform must have either a personalnumber or a
             * favorite team depending
             * Currently: Such validations are skipped due to time constraint
             */
            return _registrationForm;
        }
    }
}