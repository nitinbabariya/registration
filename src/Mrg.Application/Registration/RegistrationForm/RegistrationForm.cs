﻿using Mrg.Domain.Common;

namespace Mrg.ApplicationService.Registration.RegistrationForm
{
    public  class RegistrationForm
    {
        public Name FirstName { get;internal set; }
        public Name LastName { get; internal set; }
        public Address Address { get; internal set; }
        public PersonalNumber PersonalNumber { get; internal set; }
        public FavoriteFootballTeam FavoriteFootballTeam { get; internal set; }
    }

    /*
     *An alternative option is to have RegistrationForm as an abstract class and
     * Create two different implementation of it  one with a favorite game and one with a personnal number
     */
}