﻿using Guards;

namespace Mrg.ApplicationService.Registration
{
    public sealed class FavoriteFootballTeam
    {
        private FavoriteFootballTeam()
        {
            TeamName = string.Empty;
        }

        internal FavoriteFootballTeam(string teamName)
        {
            Guard.ArgumentNotNullOrEmpty(() => teamName);

            TeamName = teamName;
        }

        public static FavoriteFootballTeam NotSpecified => new FavoriteFootballTeam();

        public string TeamName { get; }

        public static implicit operator string(FavoriteFootballTeam favoriteFootballTeam)
        {
            return favoriteFootballTeam.TeamName;
        }

        public static implicit operator FavoriteFootballTeam(string favoriteFootballTeam)
        {
            return new FavoriteFootballTeam(favoriteFootballTeam);
        }

        public static implicit operator Domain.Common.FavoriteFootballTeam(FavoriteFootballTeam favoriteFootballTeam)
        {
            return new Domain.Common.FavoriteFootballTeam(favoriteFootballTeam);
        }
    }
}