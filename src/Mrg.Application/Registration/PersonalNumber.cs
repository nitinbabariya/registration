﻿using CSharpFunctionalExtensions;
using Guards;

namespace Mrg.ApplicationService.Registration
{
    public sealed class PersonalNumber:ValueObject<PersonalNumber>
    {
        public static PersonalNumber NotSpecified => new PersonalNumber();

        private PersonalNumber()
        {
            Number = string.Empty;
        }

        internal PersonalNumber(string number)
        {
            Guard.ArgumentNotNullOrEmpty(() => number);

            Number = number;
        }

        public string Number { get;  set; }
        protected override bool EqualsCore(PersonalNumber other) => Number == other.Number;

        protected override int GetHashCodeCore()=>Number.GetHashCode();

        public static implicit operator string(PersonalNumber personalNumber)=>personalNumber.Number;
        
        public static implicit operator PersonalNumber(string personalNumber)=>new PersonalNumber(personalNumber);
        public static implicit operator  Domain.Common.PersonalNumber(PersonalNumber personalNumber)=>new Domain.Common.PersonalNumber(personalNumber);
    }
}