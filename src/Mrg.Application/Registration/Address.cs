﻿using CSharpFunctionalExtensions;

namespace Mrg.ApplicationService.Registration
{
    public class Address : ValueObject<Address>
    {
        public string Street { get; }
        public string City { get; }
        public string Country { get; }

        public Address(string street)
        {
            Street = street;
        }

        public Address(string country,string street, string city)
        {
            Country = country;
            Street = street;
            City = city;
        }

        protected override bool EqualsCore(Address other)
        {
            return Street == other.Street && City == other.City;
        }

        protected override int GetHashCodeCore()
        {
            unchecked
            {
                int hashCode = Street.GetHashCode();
                hashCode = (hashCode * 397) ^ City.GetHashCode() ^ Country.GetHashCode();
                return hashCode;
            }
        }

        public static implicit operator Domain.Common.Address(Address address) => new Domain.Common.Address(address.Country,address.Street,address.City);
        public static implicit operator Address(Domain.Common.Address address) => new Address(address.Country,address.Street,address.City);

    }
}