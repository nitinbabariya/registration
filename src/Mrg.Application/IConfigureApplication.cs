﻿using System;
using EventFlow;

namespace Mrg.ApplicationService
{
    public interface IConfigureApplication
    {
        MrgApplication Configure(Action<IEventFlowOptions> configuration);
        ApplicationClient Run();
    }
}