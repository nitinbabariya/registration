﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading;
using Autofac;
using EventFlow;
using EventFlow.Commands;
using EventFlow.Queries;
using Mrg.ApplicationService.Client;
using Mrg.ApplicationService.Registration.RegistrationForm;
using Mrg.Domain.Registration;
using Mrg.Domain.Registration.Commands;
using Mrg.ReadStore.Registration;

namespace Mrg.ApplicationService
{
    public sealed class ApplicationClient : IDisposable
    {
        private readonly ICommandBus _commandBus;
        private readonly ILifetimeScope _container;
        private readonly IQueryProcessor _queryProcessor;

        public ApplicationClient(IQueryProcessor queryProcessor, ICommandBus commandBus, ILifetimeScope container)
        {
            _queryProcessor = queryProcessor;
            _commandBus = commandBus;
            _container = container;
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        public void Publish(params ICommand[] commands)
        {
            try
            {
                foreach (var command in commands) command.PublishAsync(_commandBus, CancellationToken.None).Wait();
            }
            catch (AggregateException e)
            {
                ExceptionDispatchInfo.Capture(e.InnerException).Throw();
            }
        }


        [DebuggerStepThrough]
        public TResult Query<TResult>(IQuery<TResult> query)
        {
            try
            {
                return _queryProcessor.ProcessAsync(query, CancellationToken.None).Result;
            }
            catch (AggregateException e)
            {
                ExceptionDispatchInfo.Capture(e.InnerException).Throw();
                throw;
            }
        }

        /// <summary>
        /// Update customer's profile such as firstname, last name, address detail
        /// </summary>
        public void UpdateCustomerProfile()
        {
            throw new NotImplementedException();
        }

        public void ChangePersonalNummber(string newPersonelNumber)
        {
            throw new NotImplementedException();
        }

        public void ChangeFavoriteFootballTeam(string newFavoriteFootballTeam)
        {
            throw new NotImplementedException();
        }

        public void DeleteCustomer(string customerId)
        {
            throw new NotImplementedException();
        }

        public RegistrationModel Enroll(RegistrationForm registrationForm)
        {
            var id = RegistrationId.New;

            var command = new EnrollRegistrationCommand(id, 
                registrationForm.FirstName, 
                registrationForm.LastName,
                registrationForm.Address, 
                registrationForm.PersonalNumber, 
                registrationForm.FavoriteFootballTeam);

            Publish(command);
            return new RegistrationModel(command.AggregateId, this);
        }

        public RegistrationModel FindRegisterationByRegistrationId(string registrationId)=> new RegistrationModel(new RegistrationId(registrationId),this);
    }
}