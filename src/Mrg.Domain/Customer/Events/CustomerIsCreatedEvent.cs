﻿using System;
using EventFlow.Aggregates;
using Mrg.Domain.Common;
using Mrg.Domain.Registration;

namespace Mrg.Domain.Customer.Events
{
    public sealed class CustomerIsCreatedEvent : AggregateEvent<CustomerAggregate, CustomerId>
    {
        public CustomerIsCreatedEvent(RegistrationId registrationId, 
            Name firstName, 
            Name lastName, 
            Address address,
            PersonalNumber personalNumber, 
            FavoriteFootballTeam favoriteFootballTeam,
            DateTime occuredOn)
        {
            RegistrationId = registrationId;
            FirstName = firstName;
            LastName = lastName;
            Address = address;
            PersonalNumber = personalNumber;
            FavoriteFootballTeam = favoriteFootballTeam;
            OccuredOn = occuredOn;
        }

        public RegistrationId RegistrationId { get; }
        public Name FirstName { get; }
        public Name LastName { get; }
        public Address Address { get; }
        public PersonalNumber PersonalNumber { get; }
        public FavoriteFootballTeam FavoriteFootballTeam { get; }
        public DateTime OccuredOn { get; }
    }
}