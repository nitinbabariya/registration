﻿using EventFlow.Aggregates;
using Mrg.Domain.Customer.Events;
using Mrg.Domain.Registration.Events;

namespace Mrg.Domain.Customer
{
    internal sealed class CustomerState : AggregateState<CustomerAggregate, CustomerId, CustomerState>,
        IApply<CustomerIsCreatedEvent>
    {
        public void Apply(RegistrationIsEnrolledEvent e)
        {
        }

        public void Apply(CustomerIsCreatedEvent aggregateEvent)
        {
            //Build state for aggregate
        }
    }
}