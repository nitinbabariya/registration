﻿using System;
using System.Threading;
using EventFlow.Aggregates;
using EventFlow.Extensions;
using Mrg.Domain.Common;
using Mrg.Domain.Customer.Events;
using Mrg.Domain.Registration;

namespace Mrg.Domain.Customer
{
    public sealed class CustomerAggregate:AggregateRoot<CustomerAggregate,CustomerId>
    {
        readonly CustomerState _state = new CustomerState();
        public CustomerAggregate(CustomerId id) : base(id)
        {
            Register(_state);
        }

        public void CreateCustomer(RegistrationId registrationId, 
            Name firstName, 
            Name lastName, 
            Address address, 
            PersonalNumber personalNumber, 
            FavoriteFootballTeam favoriteFootballTeam, 
            DateTime occuredOn, 
            CancellationToken cancellationToken)
        {
            Specs.AggregateIsNew.ThrowDomainErrorIfNotSatisfied(this);
            
            Emit(new CustomerIsCreatedEvent(registrationId, 
                firstName, 
                lastName, 
                address, 
                personalNumber,
                favoriteFootballTeam,occuredOn));
        }
    }
}