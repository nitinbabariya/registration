﻿using EventFlow.Core;
using EventFlow.ValueObjects;
using Newtonsoft.Json;

namespace Mrg.Domain.Customer
{
    [JsonConverter(typeof(SingleValueObjectConverter))]

    public sealed class CustomerId : Identity<CustomerId>
    {
        public CustomerId(string value) : base(value)
        {
        }
    }
}