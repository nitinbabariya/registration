﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;

namespace Mrg.Domain.Customer.Commands
{
    internal sealed class
        CreateCustomerCommandHandler : CommandHandler<CustomerAggregate, CustomerId, CreateCustomerCommand>
    {
        public override async Task ExecuteAsync(CustomerAggregate aggregate, CreateCustomerCommand command,
            CancellationToken cancellationToken)
        {
            aggregate.CreateCustomer(command.RegistrationId,
                command.FirstName,
                command.LastName,
                command.Address,
                command.PersonalNumber,
                command.FavoriteFootballTeam,
                command.OccuredOn,
                cancellationToken);
        }
    }
}