﻿using System;
using EventFlow.Commands;
using Mrg.Domain.Common;
using Mrg.Domain.Registration;

namespace Mrg.Domain.Customer.Commands
{
    /*
     * Alternate solution: Could have two commands one to process either one with personal number or with favorite football team
     * Following YAGNI: keeping one command as there are no different behavior for such two registrations
     */

    public sealed class CreateCustomerCommand : Command<CustomerAggregate, CustomerId>
    {
        public readonly DateTime OccuredOn;

        //TODO:Validate command
        public CreateCustomerCommand(RegistrationId registrationId,
            CustomerId aggregateId,
            Name firstName,
            Name lastName,
            Address address,
            PersonalNumber personalNumber,
            FavoriteFootballTeam favoriteFootballTeam) : base(aggregateId)
        {
            RegistrationId = registrationId;
            FirstName = firstName;
            LastName = lastName;
            Address = address;

            PersonalNumber = personalNumber;
            FavoriteFootballTeam = favoriteFootballTeam;
            OccuredOn = DateTime.UtcNow;
        }

        public RegistrationId RegistrationId { get;  }
        public Name FirstName { get;  }
        public Name LastName { get;  }
        public Address Address { get;  }
        public PersonalNumber PersonalNumber { get;  }
        public FavoriteFootballTeam FavoriteFootballTeam { get;  }
    }
}