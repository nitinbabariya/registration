﻿using EventFlow.ValueObjects;

namespace Mrg.Domain.Common
{
    public class Address : ValueObject
    {
        public Address(string street)
        {
            Street = street;
        }

        public Address(Address address) : this(address.Country, address.Street, address.City)
        {
        }

        public Address()
        {
        }

        public Address(string country, string street, string city)
        {
            Country = country;
            Street = street;
            City = city;
        }

        public string Street { get; }
        public string City { get; }
        public string Country { get; }
    }
}