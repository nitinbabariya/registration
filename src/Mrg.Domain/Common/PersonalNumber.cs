﻿using EventFlow.ValueObjects;

namespace Mrg.Domain.Common
{
    public sealed class PersonalNumber:ValueObject
    {
        public static PersonalNumber NotSpecified => new PersonalNumber();

        private PersonalNumber()
        {
        }

        public PersonalNumber(string number)
        {
            Number = number;
        }

        public string Number { get;  set; }

        public static implicit operator string(PersonalNumber personalNumber)=>personalNumber.Number;
        
        public static implicit operator PersonalNumber(string personalNumber)=>new PersonalNumber(personalNumber);
    }
}