﻿using EventFlow.ValueObjects;
using Guards;

namespace Mrg.Domain.Common
{
    public sealed class FavoriteFootballTeam:ValueObject
    {
        public static FavoriteFootballTeam NotSpecified => new FavoriteFootballTeam();

        private FavoriteFootballTeam()
        {
        }

        public FavoriteFootballTeam(string teamName)
        {
            Guard.ArgumentNotNullOrEmpty(() => teamName);

            TeamName = teamName;
        }

        public string TeamName { get; }

        public static implicit operator string(FavoriteFootballTeam favoriteFootballTeam)=> favoriteFootballTeam.TeamName;
        
        public static implicit operator FavoriteFootballTeam(string favoriteFootballTeam) => new FavoriteFootballTeam(favoriteFootballTeam);
    }
}