using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Sagas;
using EventFlow.Sagas.AggregateSagas;
using Mrg.Domain.Customer;
using Mrg.Domain.Customer.Commands;
using Mrg.Domain.Customer.Events;
using Mrg.Domain.Registration.Events;

namespace Mrg.Domain.Registration.Saga
{
    public sealed class RegistrationSaga : AggregateSaga<RegistrationSaga, RegistrationSagaId, RegistrationSagaLocator>,
        ISagaIsStartedBy<RegistrationAggregate, RegistrationId, RegistrationIsEnrolledEvent>,
        ISagaHandles<CustomerAggregate, CustomerId, CustomerIsCreatedEvent>
    {
        private readonly RegistrationSagaState _state = new RegistrationSagaState();

        public RegistrationSaga(RegistrationSagaId schedulerSagaId) : base(
            schedulerSagaId)
        {
            Register(_state);
        }

        public Task HandleAsync(IDomainEvent<CustomerAggregate, CustomerId, CustomerIsCreatedEvent> domainEvent,
            ISagaContext sagaContext, CancellationToken cancellationToken)
        {
            Complete();
            return Task.CompletedTask;
        }

        public async Task HandleAsync(
            IDomainEvent<RegistrationAggregate, RegistrationId, RegistrationIsEnrolledEvent> @event,
            ISagaContext sagaContext, CancellationToken cancellationToken)
        {
            /*To maintain/update saga state -> Emit the events
             * For example:
             * this.Emit(new RegistrationEnrollmentIsStartedEvent(@event.AggregateIdentity,
             *     @event.AggregateEvent.RegistrationDate));
             */
            var command = new CreateCustomerCommand(@event.AggregateIdentity, CustomerId.New, @event.AggregateEvent.FirstName,
                @event.AggregateEvent.LastName,
                @event.AggregateEvent.Address, @event.AggregateEvent.PersonalNumber,
                @event.AggregateEvent.FavoriteFootballTeam);

            Publish(command);
        }

        //If customer creation fails then handle retro-active events/commands to rollback the transaction
    }
}