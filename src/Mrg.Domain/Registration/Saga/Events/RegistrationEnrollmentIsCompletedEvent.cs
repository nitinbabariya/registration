﻿using EventFlow.Aggregates;

namespace Mrg.Domain.Registration.Saga.Events
{
    public sealed class RegistrationEnrollmentIsCompletedEvent : AggregateEvent<RegistrationSaga, RegistrationSagaId>
    {

        public RegistrationEnrollmentIsCompletedEvent()
        {
        }
    }
}