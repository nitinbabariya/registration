﻿using EventFlow.Sagas;
using EventFlow.ValueObjects;

namespace Mrg.Domain.Registration.Saga
{
    public sealed class RegistrationSagaId : ValueObject, ISagaId
    {
        public readonly RegistrationId RegistrationId;
        public static readonly string MetaKey = "RegistrationSagaId";

        public RegistrationSagaId(RegistrationId registrationId)
        {
            RegistrationId = registrationId;
        }

        public RegistrationSagaId(string value)
        {
            //TODO:Use compiled regex instead of string.Replace(..)
            var id = value.Replace($"{MetaKey}-", "");

            this.RegistrationId = new RegistrationId(id);
        }

        public string Value => $"{MetaKey}-"+ this.RegistrationId.Value;
    }
}