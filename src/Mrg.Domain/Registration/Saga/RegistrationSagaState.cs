using EventFlow.Aggregates;
using Mrg.Domain.Registration.Events;

namespace Mrg.Domain.Registration.Saga
{
    internal sealed class RegistrationSagaState : AggregateState<RegistrationSaga, RegistrationSagaId, RegistrationSagaState>,
        IApply<RegistrationIsEnrolledEvent>
    {
        
        public void Apply(RegistrationIsEnrolledEvent aggregateEvent)
        {
        }
    }
}