﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Sagas;
using Mrg.Domain.Registration.Events;

namespace Mrg.Domain.Registration.Saga
{
    public sealed class RegistrationSagaLocator : ISagaLocator
    {
        public RegistrationSagaId GetSagaId(string registrationId) => new RegistrationSagaId(new RegistrationId(registrationId));

        public RegistrationSagaId GetSagaId(RegistrationId registrationId) => new RegistrationSagaId($"{RegistrationSagaId.MetaKey}-{registrationId}");

        public Task<ISagaId> LocateSagaAsync(IDomainEvent domainEvent, CancellationToken cancellationToken)
        {
            var @event = domainEvent.GetAggregateEvent();
            switch (@event)
            {
                case RegistrationIsEnrolledEvent _:
                {
                    var registrationId = (RegistrationId)domainEvent.GetIdentity();
                    var sagaId = GetSagaId(registrationId);
                    return Task.FromResult<ISagaId>(sagaId);
                }
                case Customer.Events.CustomerIsCreatedEvent _customerIsCreatedEvent:
                {
                    var sagaId = GetSagaId(_customerIsCreatedEvent.RegistrationId);
                    return Task.FromResult<ISagaId>(sagaId);
                }
                default:
                {
                    var id = domainEvent.Metadata[RegistrationSagaId.MetaKey];
                    var sagaId = GetSagaId(id);

                    return Task.FromResult<ISagaId>(sagaId);
                }
            }
        }
    }
}