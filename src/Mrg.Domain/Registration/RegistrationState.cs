﻿using EventFlow.Aggregates;
using Mrg.Domain.Registration.Events;

namespace Mrg.Domain.Registration
{
    internal sealed class RegistrationState : AggregateState<RegistrationAggregate, RegistrationId, RegistrationState>,
        IApply<RegistrationIsEnrolledEvent>
    {

        public void Apply(RegistrationIsEnrolledEvent e)
        {
        }
    }
}