﻿using System;
using EventFlow.Commands;
using Mrg.Domain.Common;

namespace Mrg.Domain.Registration.Commands
{
    /*
     * Alternate solution: Could have two commands one to process either one with personal number or with favorite football team
     * Following YAGNI: keeping one command as there are no different behavior for such two registrations
     */

    public sealed class EnrollRegistrationCommand : Command<RegistrationAggregate, RegistrationId>
    {
        public readonly DateTime OccuredOn;

        //TODO:Validate command
        public EnrollRegistrationCommand(RegistrationId aggregateId,
            Name firstName,
            Name lastName,
            Address address,
            PersonalNumber personalNumber,
            FavoriteFootballTeam favoriteFootballTeam) : base(aggregateId)
        {
            FirstName = firstName;
            LastName = lastName;
            Address = address;

            PersonalNumber = personalNumber;
            FavoriteFootballTeam = favoriteFootballTeam;
            OccuredOn = DateTime.UtcNow;
        }

        public Name FirstName { get; }
        public Name LastName { get; }
        public Address Address { get; }
        public PersonalNumber PersonalNumber { get; }
        public FavoriteFootballTeam FavoriteFootballTeam { get; }
    }
}