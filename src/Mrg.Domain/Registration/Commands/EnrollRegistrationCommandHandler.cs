﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Commands;

namespace Mrg.Domain.Registration.Commands
{
    internal sealed class EnrollRegistrationCommandHandler : CommandHandler<RegistrationAggregate, RegistrationId, EnrollRegistrationCommand>
    {
        public override async Task ExecuteAsync(RegistrationAggregate aggregate, EnrollRegistrationCommand command, CancellationToken cancellationToken)
        {
            aggregate.EnrollRegistration(command.FirstName, 
                command.LastName, 
                command.Address, 
                command.PersonalNumber,
                command.FavoriteFootballTeam, 
                command.OccuredOn,
                cancellationToken);
        }
    }
}