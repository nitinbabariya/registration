﻿using System;
using System.Threading;
using EventFlow.Aggregates;
using EventFlow.Extensions;
using Mrg.Domain.Common;
using Mrg.Domain.Registration.Events;

namespace Mrg.Domain.Registration
{
    public sealed class RegistrationAggregate:AggregateRoot<RegistrationAggregate,RegistrationId>
    {
        readonly RegistrationState _state = new RegistrationState();
        public RegistrationAggregate(RegistrationId id) : base(id)
        {
            Register(_state);
        }

        public void EnrollRegistration(Name firstName, 
            Name lastName, 
            Address address, 
            PersonalNumber personalNumber, 
            FavoriteFootballTeam favoriteFootballTeam, 
            DateTime occuredOn, 
            CancellationToken cancellationToken)
        {
            Specs.AggregateIsNew.ThrowDomainErrorIfNotSatisfied(this);
            
            Emit(new RegistrationIsEnrolledEvent(firstName, 
                lastName, 
                address, 
                personalNumber,
                favoriteFootballTeam, 
                occuredOn));
        }
    }
}