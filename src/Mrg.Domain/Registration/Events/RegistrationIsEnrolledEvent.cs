﻿using System;
using EventFlow.Aggregates;
using Mrg.Domain.Common;

namespace Mrg.Domain.Registration.Events
{
    public sealed class RegistrationIsEnrolledEvent : AggregateEvent<RegistrationAggregate, RegistrationId>
    {
        public RegistrationIsEnrolledEvent(Name firstName,
            Name lastName,
            Address address,
            PersonalNumber personalNumber,
            FavoriteFootballTeam favoriteFootballTeam,
            DateTime occuredOn)
        {
            FirstName = firstName;
            LastName = lastName;
            Address = new Address(address);
            PersonalNumber = personalNumber;
            FavoriteFootballTeam = favoriteFootballTeam;

            Occured = occuredOn;
            Noticed = DateTime.UtcNow;
        }

        public Name FirstName { get; internal set; }
        public Name LastName { get; internal set; }
        public Address Address { get; internal set; }
        public PersonalNumber PersonalNumber { get; internal set; }
        public FavoriteFootballTeam FavoriteFootballTeam { get; internal set; }

        //https://martinfowler.com/eaaDev/DomainEvent.html
        //actual occured date of event in real world
        public DateTime Occured { get; }

        //Time when event is transited/handled in system
        public DateTime Noticed { get; }
    }
}