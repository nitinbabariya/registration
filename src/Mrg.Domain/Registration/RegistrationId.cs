﻿using EventFlow.Core;
using EventFlow.ValueObjects;
using Newtonsoft.Json;

namespace Mrg.Domain.Registration
{
    [JsonConverter(typeof(SingleValueObjectConverter))]

    public sealed class RegistrationId : Identity<RegistrationId>
    {
        public RegistrationId(string value) : base(value)
        {
        }
    }
}