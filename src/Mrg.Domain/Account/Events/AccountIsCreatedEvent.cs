﻿using System;
using EventFlow.Aggregates;
using Mrg.Domain.Common;

namespace Mrg.Domain.Account.Events
{
    public sealed class AccountIsCreatedEvent : AggregateEvent<AccountAggregate, AccountId>
    {
        public AccountId Id { get; }
        public Name Name { get; }
        public DateTime Occured{ get; }
        public DateTime Noticed{ get; }

        public AccountIsCreatedEvent(AccountId customerId, Name name) 
        {
            Id = customerId;
            Name = name;
            Noticed = DateTime.UtcNow;
        }
    }
}