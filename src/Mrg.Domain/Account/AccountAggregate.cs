﻿using EventFlow.Aggregates;
using EventFlow.Extensions;
using Mrg.Domain.Account.Events;

namespace Mrg.Domain.Account
{
    public sealed class AccountAggregate:AggregateRoot<AccountAggregate,AccountId >
    {
        readonly AccountState _state = new AccountState();
        public AccountAggregate(AccountId id) : base(id)
        {
            Register(_state);
        }

        public void CreateAccount(string name)
        {
            Specs.AggregateIsNew.ThrowDomainErrorIfNotSatisfied(this);
            Emit(new AccountIsCreatedEvent(Id, name));
        }
    }
}