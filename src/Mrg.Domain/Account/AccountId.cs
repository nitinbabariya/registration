﻿using EventFlow.Core;
using EventFlow.ValueObjects;
using Newtonsoft.Json;

namespace Mrg.Domain.Account
{
    [JsonConverter(typeof(SingleValueObjectConverter))]

    public sealed class AccountId : Identity<AccountId >
    {
        public AccountId (string value) : base(value)
        {
        }
    }
}