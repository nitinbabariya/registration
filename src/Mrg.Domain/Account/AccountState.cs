﻿using System;
using EventFlow.Aggregates;
using Mrg.Domain.Account.Events;

namespace Mrg.Domain.Account
{
    internal sealed class AccountState : AggregateState<AccountAggregate, AccountId, AccountState>,
        IApply<AccountIsCreatedEvent>
    {
        public void Apply(AccountIsCreatedEvent aggregateEvent)
        {
          //  RegistrationDate = e.RegistrationDate;
        }
    }
}