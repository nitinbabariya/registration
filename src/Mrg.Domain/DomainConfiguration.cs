﻿using System.Reflection;
using EventFlow;
using EventFlow.Extensions;

namespace Mrg.Domain
{
    public static class DomainConfiguration
    {
        public static Assembly DomainAssembly => typeof(DomainConfiguration).GetTypeInfo().Assembly;

        public static IEventFlowOptions AddDomain(this IEventFlowOptions options)
        {
            return options.AddDefaults(DomainAssembly);
        }
    }
}
