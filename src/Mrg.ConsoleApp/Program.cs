﻿using System;
using EventFlow.Core;
using Mrg.ApplicationService;
using Mrg.ApplicationService.Registration.RegistrationForm;

namespace Mrg.ConsoleApp
{
   class Program
    {
        static void Main(string[] args)
        {
            using (var client = MrgApplication.Create().ConfigureInMemoryStore().Run())
            {
                var registrationForm = RegistrationFormBuilder.Begin("John", "Doe", "Main street, stockholm")
                    .WithFavoriteFootballTeam(" Liverpool FC").Build();

                var response= client.Enroll(registrationForm);
                Console.WriteLine($"Registration is enrolled with Id: {response.Id}");

                var customer=  client.FindRegisterationByRegistrationId(response.Id.ToString());

                Console.WriteLine($"{new JsonSerializer().Serialize(customer,true)}");
                
                Console.WriteLine("\n\tExcellent!!");
            }

            Console.ReadKey();
        }
    }
}
