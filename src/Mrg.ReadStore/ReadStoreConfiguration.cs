﻿using System.Reflection;
using EventFlow;
using EventFlow.Extensions;

namespace Mrg.ReadStore
{
    public static class ReadStoreConfiguration
    {
        public static Assembly GetAssembly => typeof(ReadStoreConfiguration).GetTypeInfo().Assembly;
    }
}
