﻿using EventFlow.Aggregates;
using EventFlow.ReadStores;
using Mrg.Domain.Common;
using Mrg.Domain.Registration;
using Mrg.Domain.Registration.Events;

namespace Mrg.ReadStore.Registration
{
    public sealed class RegistrationReadModel : IReadModel,
        IAmReadModelFor<RegistrationAggregate, RegistrationId, RegistrationIsEnrolledEvent>
    {
        public RegistrationId Id{ get; private set; }
        public Name FirstName { get;internal set; }
        public Name LastName { get; internal set; }
        public Address Address { get; internal set; }
        public PersonalNumber PersonalNumber { get; internal set; }
        public FavoriteFootballTeam FavoriteFootballTeam { get; internal set; }

        public void Apply(IReadModelContext context, IDomainEvent<RegistrationAggregate, RegistrationId, RegistrationIsEnrolledEvent> domainEvent)
        {
            Id = domainEvent.AggregateIdentity;
            FirstName = domainEvent.AggregateEvent.FirstName;
            LastName = domainEvent.AggregateEvent.LastName;
            Address = domainEvent.AggregateEvent.Address;
            PersonalNumber = domainEvent.AggregateEvent.PersonalNumber;
            FavoriteFootballTeam = domainEvent.AggregateEvent.FavoriteFootballTeam;
        }
    }
}